package mo.said.seniorenuhr;

import static androidx.core.content.PermissionChecker.PERMISSION_GRANTED;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.example.tablet.R;

//import com.tomerrosenfeld.*

public class MainActivity extends AppCompatActivity {

    final int callbackId = 42;

    private MyModel myModel;
    private MyView myView;



    private void checkPermission(int callbackId, String... permissionsId) {
        boolean permissions = true;
        for (String p : permissionsId) {
            permissions = permissions && ContextCompat.checkSelfPermission(this, p) == PERMISSION_GRANTED;
        }

        if (!permissions) {
            ActivityCompat.requestPermissions(this, permissionsId, callbackId);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        checkPermission(callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        analogSwitch();


        myModel = new MyModel(this);
        myView = new MyView(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        init();
        myView.init();
//        myView.switcher();


    }

    public void init() {



        myModel.addView("alles", findViewById(R.id.alles));

        myModel.addImageView("theme", findViewById(R.id.theme));
        myModel.addImageView("analog", findViewById(R.id.analog));

        myModel.addTextView("day", findViewById(R.id.day));
        myModel.addTextView("date", findViewById(R.id.date));
        myModel.addTextView("time", findViewById(R.id.time));
        myModel.addTextView("termine", findViewById(R.id.termine));
        myModel.addTextView("termine2", findViewById(R.id.termine2));
        myModel.addTextView("termine3", findViewById(R.id.termine3));
        myModel.addTextView("abschnitt", findViewById(R.id.abschnitt));


        myView.subscribe(myModel);
        myModel.subscribe(myView);
    }




    public void analogSwitch(View v) {

        Intent intent = new Intent(this, AnalogActivity.class);
//        FullscreenActivity.farbe = this.myView.farbe;
        startActivity(intent);
        finish();
//        setContentView(R.layout.activity_fullscreen);
    }


    public void analogSwitch() {

//        setContentView(R.layout.activity_fullscreen);

        Intent intent = new Intent(this, AnalogActivity.class);
        startActivity(intent);

//        setContentView(R.layout.activity_fullscreen);
    }


}